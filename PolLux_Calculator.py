# -*- coding: utf-8 -*-
"""
Created on Tue Dec 05 10:48:38 2017

@author: witte_k, watts
"""

__version__ = '0.8.0'

try:
	#Python 3 imports
	import tkinter as tk
	import tkinter.ttk as ttk
	import tkinter.filedialog
	from tkinter.font import Font
except ImportError:
	#Python 2 imports
	import Tkinter as tk
	import ttk
	import tkFileDialog
	from tkFont import Font

import numpy as np
#import scipy as sp
import h5py
from functools import partial
from scipy.constants import *
import scipy.special, scipy.ndimage, scipy.interpolate, scipy.signal
import matplotlib
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.figure import Figure
from matplotlib import pyplot

try:
	# Close the splash screen in compiled versions.
	import pyi_splash
	pyi_splash.close()
except:
	pass


def call_calc(Result_Wavelength, Result_Nzones, Result_FocLen, Result_DOF, E_input_eV, Dia_input_um, OZW_input_nm, DiffOrder_input_int):

	E = E_input_eV.get()
	dia = Dia_input_um.get()
	drn = OZW_input_nm.get()
	diffo = DiffOrder_input_int.get()
	
	#calculate wavelength in nm
	nm = float(((h*c)/e*1e09)/float(E))
	Result_Wavelength.config(text="Wavelength: %5.2f nm" % nm)
	
	#calculate No of zones
	N = float((float(dia)*10**-6)/(4*float(drn)*(10**-9)))
	Result_Nzones.config(text="No of zones: %.0f" % N)
		
	#calculate focal length in mum
	nm = float(((h*c)/e*1e09)/float(E))
	N = float((float(dia)*10**-6)/(4*float(drn)*(10**-9)))
	f = float((((4*N*float(drn)**2)/nm)/1e06)/float(diffo))
	Result_FocLen.config(text="Focal length: %5.2f mm" % f)

	#calculate depth of focus DOF:
	nm = float(((h*c)/e*1e09)/float(E))
	dz = float(((2*(float(drn)**2))/nm)*0.002/float(diffo))
	Result_DOF.config(text=u"DOF: %5.2f \u03BCm" % dz)


def ResPower(Result_ResPow, Result_EnergyRes, E_input_eV,DiffOrder_input_int,HExitSlit_input_um,HEntrSlit_input_um,grating_params):
	E = E_input_eV.get()
	diffo = 1. #Mono, not ZP!!!
	ExitSlit_hor = HExitSlit_input_um.get()
	EntranceSlit_hor = HEntrSlit_input_um.get()
	LineDensity = grating_params['LineDensity']
	DeflAngle = grating_params['DeflectionAngle']
	GratingRadius = grating_params['GratingRadius']
	Illu = grating_params['Illumination']
	S1=grating_params['S1']
	S2=grating_params['S2']
	
	nm = float(((h*c)/e*1e09)/float(E))*1e-09
	d = float(1.0e-3/float(LineDensity))                                 #Grating period low Energy grating
	theta=np.radians(DeflAngle*0.5)
	phi=np.arcsin(int(diffo)*nm/(d*2.0*np.cos(theta)))
	alpha=phi+theta                                                     #Grating angles alpha & beta
	beta=phi-theta
	
	f1 = float((np.cos(alpha)**2/S1-np.cos(alpha)/GratingRadius) + (np.cos(beta)**2/S2-np.cos(beta)/GratingRadius))
	nm_Err1 = float((1.0e-3/float(LineDensity))/int(diffo)*Illu*abs(f1))
	nm_Err2 = float( 1.0e-3/float(LineDensity)/int(diffo)*(1e-6*float(EntranceSlit_hor)/S1*abs(np.cos(alpha))+1e-6*float(ExitSlit_hor)/S2*abs(np.cos(beta))))
	
	RP=nm/np.sqrt(nm_Err1**2+nm_Err2**2)                                  #resolving power low E grating
	DE=float(E)/RP                                                          #Delta E low E grating
	
	Result_ResPow.config(text="Resolving power: %4i" % RP)
	Result_EnergyRes.config(text=u"\u0394E: %1.2f eV" % DE)
	return RP

### Calculate PSF and convolutions with broadening effects
def focal_length(energy,diameter,zone_width,diff_order):
	return diameter/float(diff_order)/(2*np.tan(np.arcsin(1239.842e-9/energy*.5/zone_width)))   #% Focal length in [m]
	
def diff_limited_PSF(x,y,energy,diameter,zone_width,diff_order,CS_diameter):
	"""Calculate the intensity of the diffraction limited Point Spread Function in the focal plane"""
	wavelength = 1239.842e-9/energy                          #% Wavelength in [m]
	k = 2*np.pi/wavelength                                  #% Wave vector
	f = (diameter/float(diff_order)/(2*np.tan(np.arcsin(wavelength*.5/zone_width))))   #% Focal length in [m]
	a = diameter*.5                                           #% Radius in [m]
	CSF = CS_diameter/diameter                                # central stop fraction
	denominator = k*a*np.sqrt(x**2+y**2)/f
	Outer_Bessel = scipy.special.jv(1,(    k*a*np.sqrt(x**2+y**2)/f))
	Inner_Bessel = scipy.special.jv(1,(CSF*k*a*np.sqrt(x**2+y**2)/f))
	#PSF = np.divide(2*scipy.special.jv(1,(k*a*np.sqrt(x**2+y**2)/f)),denominator,where=denominator!=0)**2
	PSF = (1-CSF**2)**(-2)*np.divide(2*Outer_Bessel-2*CSF*Inner_Bessel,denominator,where=denominator!=0)**2
	PSF[denominator==0] = 1.
	return PSF

def RayBundleHyperbola(z_points,energy,diameter,zone_width,diff_order):
	""" Model expansion of ray bundle in out-of-focus planes by a hyperbola defined by:
	 * symmetric about focal plane
	 * vertex at center of the focal plane
	 * asymptotes passing through outer edge of zone plate and a point on the zp-axis 4 focal-depths (the "half" version of DOF) before the focal plane
	 
	 This is only an approximation. I think that the only correct way to do this would be a diffraction simulation.
	 
	 The z_points parameter (float value or vector numpy array) are positions (in m) along the optical axis with the origin at the focal plane
	 An array matching the size of z_points is returned containing the full width of the diverged ray bundle.
	"""
	f = focal_length(energy,diameter,zone_width,diff_order)
	DOF = float(4*(float(zone_width)**2)*energy/1239.842e-9/float(diff_order))
	F = 2*DOF #(DOF is the full width of the focus field)
	hyp_a = F*diameter*.5/(f-F)
	hyp_b = -F
	hyp_k = -F*.5*diameter/(f-F)
	return 2*(hyp_a*np.sqrt((z_points/hyp_b)**2+1)+hyp_k)

def ChromaticBroadening(x_PSF,base_PSF,energy,diameter,zone_width,diff_order,RP,N_E=5):
	""" Apply broadening of PSF due to imperfect spectral resolution of the zone plate illumination."""
	x_PSF_step = float(x_PSF[1]-x_PSF[0])
	base_weight = np.sum(base_PSF)
	base_f = focal_length(energy,diameter,zone_width,diff_order)
	DE = energy/float(RP)
	Ei = np.linspace(energy-DE,energy+DE,2*N_E+1)  # The quoted energy resolution represents the FWHM of a normal distribution
	Ei_weights = np.exp(-((Ei-energy)/float(DE/2.355))**2)
	Ei_weights = Ei_weights/np.sum(Ei_weights)
	final_PSF = base_PSF*0.
	for i in range(len(Ei)):
		df = focal_length(Ei[i],diameter,zone_width,diff_order)-base_f
		broadening = RayBundleHyperbola(df,Ei[i],diameter,zone_width,diff_order)
		conv_pix = int(round(broadening/x_PSF_step))
		if conv_pix > len(base_PSF):
			print('Need to calculate PSF over wider range!')
			ext_pix = int(conv_pix/2)
			convolved_PSF = np.convolve(np.pad(base_PSF,ext_pix,mode='constant',constant_values=0), np.ones(conv_pix), mode='same')
			convolved_PSF = convolved_PSF[ext_pix:-ext_pix]/np.sum(convolved_PSF)*base_weight*Ei_weights[i]
		elif conv_pix > 0:
			convolved_PSF = np.convolve(base_PSF, np.ones(conv_pix), mode='same')
			convolved_PSF = convolved_PSF/np.sum(convolved_PSF)*base_weight*Ei_weights[i]
		else:
			convolved_PSF = base_PSF*Ei_weights[i]
		#pylab.plot(x_PSF*1e9,convolved_PSF, label=str(i))
		final_PSF += convolved_PSF
	#pylab.plot(x_PSF*1e9,final_PSF, label='convolved')
	#print(np.sum(final_PSF))
	return final_PSF

def SourceBroadening(x_PSF,base_PSF,energy,diameter,zone_width,diff_order,secondary_source_width,source_distance):
	f = focal_length(energy,diameter,zone_width,diff_order)
	source_image = secondary_source_width*f/float(source_distance)
	x_PSF_step = float(x_PSF[1]-x_PSF[0])
	conv_pix = int(round(source_image/x_PSF_step))
	base_weight = np.sum(base_PSF)
	if conv_pix > len(base_PSF):
		print('Need to calculate PSF over wider range!')
		ext_pix = int(conv_pix/2)
		convolved_PSF = np.convolve(np.pad(base_PSF,ext_pix,mode='constant',constant_values=0), np.ones(conv_pix), mode='same')
		convolved_PSF = convolved_PSF[ext_pix:-ext_pix]/np.sum(convolved_PSF)*base_weight
	elif conv_pix > 0:
		convolved_PSF = np.convolve(base_PSF, np.ones(conv_pix), mode='same')
		convolved_PSF = convolved_PSF/np.sum(convolved_PSF)*base_weight
	else:
		convolved_PSF = base_PSF
	return convolved_PSF

def radius_image(Npix,psflim):
	#Npix = 100
	xi = np.linspace(-psflim,psflim,int(Npix))
	[xx,yy] = np.meshgrid(xi,xi)
	im = np.sqrt(xx**2+yy**2)

	return im


class GUIApplication(tk.Frame):
	def __init__(self, parent, *args, **kwargs):
		tk.Frame.__init__(self, parent, *args, **kwargs)
		self.parent = parent
		########################################## Display #############################################
		parent.geometry('660x610+100+100')
		parent.title('PolLux Calculator  v'+__version__)
		parent.config(background = 'cornflower blue')
		text=tk.Text(self)
		myFont = ("Helvetica", 10)
		titleFont = Font(family="Helvetica", size=12)
		text.config(font=myFont)
		#LARGE_FONT = ("Verdana", 12)



		self.E_input_eV = tk.StringVar()       # energy
		self.Dia_input_um = tk.StringVar()       # diameter
		self.Dia_input_um.set(240)
		self.CSDia_input_um = tk.StringVar()       # central stop diameter
		self.CSDia_input_um.set(80)
		self.OZW_input_nm = tk.StringVar()       # drn
		self.OZW_input_nm.set(25)
		self.DiffOrder_input_int = tk.StringVar()       # diffo
		self.DiffOrder_input_int.set(1)

		title_ZP_Params = tk.Label(root, text ="Zoneplate Parameters", background = 'mint cream', font = titleFont)
		title_ZP_Params.grid(row = 0, column = 0, columnspan = 2)

		#Creating Input Options 
		label_E = tk.Label(root, text="Energy / eV", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.E_input_eV)
		label_E.grid(row=1)
		self.input_entry.grid(row=1, column=1)

		label_Dia = tk.Label(root, text=u"Diameter / \u03BCm", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.Dia_input_um)
		label_Dia.grid(row=2)
		self.input_entry.grid(row=2, column=1)

		label_CSD = tk.Label(root, text=u"Central Stop Diameter / \u03BCm", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.CSDia_input_um)
		label_CSD.grid(row=3)
		self.input_entry.grid(row=3, column=1)

		label_ozw = tk.Label(root, text="Outer zone width / nm", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.OZW_input_nm)
		label_ozw.grid(row=4)
		self.input_entry.grid(row=4, column=1)

		label_DiffOrd = tk.Label(root, text="Diffraction order", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.DiffOrder_input_int)
		label_DiffOrd.grid(row=5)
		self.input_entry.grid(row=5, column=1)

		labelSpace2 = tk.Label(root, text="", background = 'cornflower blue', font = myFont)
		labelSpace2.grid(row=6, column=1)

		# Creating result labels
		self.Result_Wavelength = tk.Label(root, text = "Wavelength", background = 'cornflower blue', font = myFont)
		self.Result_Wavelength.grid(row=1, column=4 , columnspan=6)
		self.Result_Nzones = tk.Label(root, text = "No of zones", background = 'cornflower blue', font = myFont)
		self.Result_Nzones.grid(row=2, column=4, columnspan=6)
		self.Result_FocLen = tk.Label(root, text = "Focal length", background = 'cornflower blue', font = myFont)
		self.Result_FocLen.grid(row=3, column=4, columnspan=6)
		self.Result_DOF = tk.Label(root, text = "DOF", background = 'cornflower blue', font = myFont)
		self.Result_DOF.grid(row=4, column=4, columnspan=6)
		self.Result_DiffLimitRes = tk.Label(root, text = "Diffr. limited resolution / nm", background = 'cornflower blue', font = myFont)
		self.Result_DiffLimitRes.grid(row=5, column=4, columnspan=6)

		######################################## Source & Spot Calculations #######################################


		self.HExitSlit_input_um = tk.StringVar()                 # hor. exit slit
		self.VExitSlit_input_um = tk.StringVar()                 # vert. exit slit
		self.SD_input_m = tk.StringVar()                 # source distance
		self.SD_input_m.set(1)

		label_HExS = tk.Label(root, text=u"Hor. exit slit / \u03BCm", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.HExitSlit_input_um)
		label_HExS.grid(row=16, column=0)
		self.input_entry.grid(row=16, column=1)

		label_VExS = tk.Label(root, text=u"Vert. exit slit / \u03BCm", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.VExitSlit_input_um)
		label_VExS.grid(row=17, column=0)
		self.input_entry.grid(row=17, column=1)

		label_SD = tk.Label(root, text="Source distance / m", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.SD_input_m)
		label_SD.grid(row=18, column=0)
		self.input_entry.grid(row=18, column=1)

		# Creating result labels
		self.Result_WorkDist = tk.Label(root, text = "Working distance of ZP", background = 'cornflower blue', font = myFont)
		self.Result_WorkDist.grid(row=18, column=4, columnspan = 6)

		self.Result_Hspot = tk.Label(root, text = "Hor. spot size / nm", background = 'cornflower blue', font = myFont)
		self.Result_Hspot.grid(row=16, column=4, columnspan = 6)

		self.Result_Vspot = tk.Label(root, text = "Vert. spot size / nm", background = 'cornflower blue', font = myFont)
		self.Result_Vspot.grid(row=17, column=4, columnspan = 6)

		self.Result_Demag = tk.Label(root, text = "Demagnification", background = 'cornflower blue', font = myFont)
		self.Result_Demag.grid(row=19, column=4, columnspan = 6)

		labelSpace3 = tk.Label(root, text = "", background = 'cornflower blue')
		labelSpace3.grid(row=20, column=0)


		self.HEntrSlit_input_um = tk.StringVar()                 # hor. entrance slit
		self.HEntrSlit_input_um.set(200)

		######################################## Energy Resolution #######################################
		### Define Beamline Parameters:
		# 'LineDensity':      number of lines per mm ruled into the grating
		# 'DeflectionAngle':  change in beam path direction, in degrees
		# 'S1':               distance between grating and entrance slit, in metres
		# 'S2':               distance between grating and exit slit, in metres
		# 'GratingRadius':    radius of curvature of grating, in metres
		# 'Illumination':     footprint of beam on grating, in metres
		# 'd':                Grating period

		PolLuxLowEGrating    = {'LineDensity':300, 'DeflectionAngle':175.035, 'S1':3.5, 'S2':6.5, 'GratingRadius':114.9, 'Illumination':0.04}#Ni
		PolLuxHighEGrating   = {'LineDensity':600, 'DeflectionAngle':175.035, 'S1':3.5, 'S2':6.5, 'GratingRadius':114.7, 'Illumination':0.04}#Au
		self.mono_gratings = {'Ni 300 l/mm (Low Energy)':PolLuxLowEGrating, 'Au 600 l/mm (High Energy)':PolLuxHighEGrating}

		self.input_title_Eresol = tk.Label(root, text="Beamline Parameters", background = 'mint cream', font = titleFont)
		self.input_title_Eresol.grid(row = 11, column = 0, columnspan = 2)

		label_HEnS = tk.Label(root, text=u"Hor. entrance slit / \u03BCm", background = 'cornflower blue', font = myFont)
		self.input_entry = tk.Entry(root, textvariable=self.HEntrSlit_input_um)
		label_HEnS.grid(row=12, column=0)
		self.input_entry.grid(row=12, column=1)

		label_grating = tk.Label(root, text="Monochromator grating", background = 'cornflower blue', font = myFont)
		self.combobox = ttk.Combobox(root, textvariable=tk.StringVar())
		label_grating.grid(row=14, column=0)
		self.combobox.grid(row=14, column=1)
		self.combobox['values'] = (list(self.mono_gratings.keys()))
		self.combobox['state'] = 'readonly'
		self.combobox.set(list(self.mono_gratings.keys())[0])



		# Create result label
		self.Result_ResPow = tk.Label(root, text = "Resolving power", background = 'cornflower blue', font = myFont)
		self.Result_ResPow.grid(row=12, column=4, columnspan = 6)


		self.Result_EnergyRes = tk.Label(root, text = u"\u0394E", background = 'cornflower blue', font = myFont)
		self.Result_EnergyRes.grid(row=14, column=4, columnspan = 6)



		### Insert plots

		matplotlib.rcParams.update({'font.size': 6})
		self.fig = Figure(figsize=(6.5, 3), dpi=100)
		t = np.arange(0, 3, .01)
		self.ax1 = self.fig.add_subplot(121)
		self.ax2 = self.fig.add_subplot(122)
		mappable = self.ax2.imshow(np.zeros((10,10)))
		cbar = self.fig.colorbar(mappable)

		self.canvas = FigureCanvasTkAgg(self.fig, master=root)  # A tk.DrawingArea.
		self.canvas.draw()
		self.canvas.get_tk_widget().grid(row=21, column=0, columnspan = 11)#.pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)

		#toolbar = NavigationToolbar2Tk(self.canvas, root)
		#toolbar.update()
		#self.canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)


		root.bind("<Return>", self.Finale_source)
		#ent.pack(side=TOP)
		#result_button = tk.Button(root, text="Calculate", command=(lambda: Finale_source()))
		result_button = tk.Button(parent, text="Calculate", command=self.Finale_source)
		result_button.grid(row=0, column=4, columnspan=4)
			
		#browse_button = tk.Button(root, text="Load .hdf5", command=Browse_button)
		#browse_button.grid(row=0, column=8, columnspan=4)



		self.E_input_eV.set(300)
		self.HExitSlit_input_um.set(20)
		self.VExitSlit_input_um.set(20)
		self.Finale_source()


	def Finale_source(self, event=None):
		call_calc(self.Result_Wavelength, self.Result_Nzones, self.Result_FocLen, self.Result_DOF, self.E_input_eV, self.Dia_input_um, self.OZW_input_nm, self.DiffOrder_input_int)
		self.call_val(self.Result_WorkDist, self.Result_Hspot, self.Result_Vspot, self.Result_Demag, self.E_input_eV, self.Dia_input_um, self.OZW_input_nm, self.DiffOrder_input_int, self.HExitSlit_input_um, self.VExitSlit_input_um, self.SD_input_m)
		ResPower(self.Result_ResPow, self.Result_EnergyRes, self.E_input_eV,self.DiffOrder_input_int,self.HExitSlit_input_um,self.HEntrSlit_input_um,self.mono_gratings[self.combobox.get()])
		self.draw_PSF_Plots()
	
	
	def calcWorkingDistance(self, E, dia, drn, diffo, SD):
		# Calculate ZP working distance WD
		nm = float(((h*c)/e*1e09)/float(E))
		N = float((float(dia)*10**-6)/(4*float(drn)*(10**-9)))
		f = float((((4*N*float(drn)**2)/nm)/1e06)/float(int(diffo)))
		return float(1/((1/(f*10**-3))-(1/float(SD))))*1000
		Result_WorkDist.config(text="Working dist.: %5.2f mm" % WD)

		
	def call_val(self,Result_WorkDist, Result_Hspot, Result_Vspot, Result_Demag, E_input_eV, Dia_input_um, OZW_input_nm, DiffOrder_input_int, HExitSlit_input_um, VExitSlit_input_um, SD_input_m):
		E = E_input_eV.get()               # in eV
		dia = Dia_input_um.get()             # in mum
		drn = OZW_input_nm.get()             # in nm
		diffo = DiffOrder_input_int.get()
		sh = HExitSlit_input_um.get()             # hor. source size (exit slit)
		sv = VExitSlit_input_um.get()             # vert. source size (exit slit)
		SD = SD_input_m.get()             # source distance

	# Calculate ZP working distance WD
		nm = float(((h*c)/e*1e09)/float(E))
		N = float((float(dia)*10**-6)/(4*float(drn)*(10**-9)))
		f = float((((4*N*float(drn)**2)/nm)/1e06)/float(int(diffo)))
		WD = float(1/((1/(f*10**-3))-(1/float(SD))))*1000
		#WD = self.calcWorkingDistance(E, dia, drn, diffo, SD)
		Result_WorkDist.config(text="Working dist.: %5.2f mm" % WD)

	# Calculate spot size
		diff_lim = float(1.22*float(drn))/float(int(diffo))
		sph = np.sqrt(diff_lim**2 + (f*float(sh)/float(SD))**2)                 # horizontal spot size
		spv = np.sqrt(diff_lim**2 + (f*float(sv)/float(SD))**2)                 # vertical spot size

		self.Result_DiffLimitRes.config(text="Diffr. limited resol. : %5.1f nm" % diff_lim)
		Result_Hspot.config(text="Hor. spot size : %5.1f nm" % sph)
		Result_Vspot.config(text="Vert. spot size : %5.1f nm" % spv)

	# Calculate Demagnification
		DM = float(float(SD)/(f*1e-03))
		Result_Demag.config(text="Demagnification : %5.f" % DM)


	def Browse_button(self):
		global folder_path
		filename = tkFileDialog.askdirectory()
		folder_path.set(filename)
		folder_path = StringVar()

	def draw_PSF_Plots(self):
		Npix = 1e4                                          # Number of pixels
		self.ax1.clear()
		
		energy = float(self.E_input_eV.get())               # in eV
		diameter = float(self.Dia_input_um.get())*1e-6             # in mum
		CS_diameter = float(self.CSDia_input_um.get())*1e-6             # in mum	
		zone_width = float(self.OZW_input_nm.get())*1e-9             # in nm
		diff_order = int(self.DiffOrder_input_int.get())
		se = float(self.HEntrSlit_input_um.get())*1e-6                         # hor. entrance slit size
		sh = float(self.HExitSlit_input_um.get())*1e-6             # hor. source size (exit slit)
		sv = float(self.VExitSlit_input_um.get())*1e-6             # vert. source size (exit slit)
		source_distance = float(self.SD_input_m.get())             # source distance
		#N = int(diameter/(4*zone_width))            # number of zones
		
		diff_lim = float(1.22*float(zone_width)/float(diff_order))
		f = focal_length(energy,diameter,zone_width,diff_order)
		RP = ResPower(self.Result_ResPow, self.Result_EnergyRes, self.E_input_eV,self.DiffOrder_input_int,self.HExitSlit_input_um,self.HEntrSlit_input_um,self.mono_gratings[self.combobox.get()])
		energy_broadening_estimate = .5*RayBundleHyperbola(f-focal_length(energy*(1-.5/RP),diameter,zone_width,diff_order),energy*(1-.5/RP),diameter,zone_width,diff_order)
		psflim = 2*np.sqrt(diff_lim**2+(np.maximum(sh,sv)*f/source_distance)**2+energy_broadening_estimate**2)               # PSF limits in [m]
		#print(diff_lim,(np.maximum(sh,sv)*f/source_distance),energy_broadening_estimate)
		xi = np.linspace(-psflim,psflim,int(Npix))
		
		diff_lim_PSF = diff_limited_PSF(xi,0,energy,diameter,zone_width,diff_order,CS_diameter)
		self.ax1.plot(xi*1e9,diff_lim_PSF, label='Diff. Limit')# (N={})'.format(N))
		CA_PSF = ChromaticBroadening(xi,diff_lim_PSF,energy,diameter,zone_width,diff_order,RP)
		
		if abs(sh-sv) < 1e-8:  #if slits are equal
			base_PSF = SourceBroadening(xi,diff_lim_PSF,energy,diameter,zone_width,diff_order,sh,source_distance)
			self.ax1.plot(xi*1e9,SourceBroadening(xi,diff_lim_PSF,energy,diameter,zone_width,diff_order,sh,source_distance), label='Source Image')
			self.ax1.plot(xi*1e9,SourceBroadening(xi,CA_PSF,energy,diameter,zone_width,diff_order,sh,source_distance), label='Chrom. Aberr.')
		else:  #if slits are different
			H_PSF = SourceBroadening(xi,CA_PSF,energy,diameter,zone_width,diff_order,sh,source_distance)
			V_PSF = SourceBroadening(xi,CA_PSF,energy,diameter,zone_width,diff_order,sv,source_distance)
			total = np.max(H_PSF)+np.max(V_PSF)
			area_1 = np.sum(H_PSF)+np.sum(V_PSF)
			H_PSF = H_PSF/np.max(H_PSF)*total*.5  # Equalise peak heights
			V_PSF = V_PSF/np.max(V_PSF)*total*.5
			area_2 = np.sum(H_PSF)+np.sum(V_PSF)  # Ensure total 2D peak area is maintained
			self.ax1.plot(xi*1e9,H_PSF*area_1/area_2, label='Horizontal')
			self.ax1.plot(xi*1e9,V_PSF*area_1/area_2, label='Vertical')
			
		
		self.ax1.legend(loc=2)
		self.ax1.set_title("Point Spread Function")
		self.ax1.set_ylabel("Intensity")
		self.ax1.set_xlabel("Lateral Position [nm]")
		self.canvas.draw()
		
		Npix = 1000
		im = radius_image(Npix,psflim)
		PSF_interp_function = scipy.interpolate.interp1d(xi, CA_PSF, kind='cubic', bounds_error=False, fill_value=0, assume_sorted=False)
		im = PSF_interp_function(im)
		source_image_size = np.array((sh,sv))*f/float(source_distance)
		source_image_pixels = np.round(source_image_size/(psflim*2/float(Npix-1)))
		source_image_2D = np.ones(np.maximum(1,source_image_pixels[::-1].astype(int))) #reverse source_image_pixels to get correct image orientation
		source_image_2D = source_image_2D/np.sum(source_image_2D)
		im = scipy.signal.convolve(im, source_image_2D, mode='same', method='auto')
		mappable = self.ax2.imshow(im,extent=1e9*np.array([-psflim, psflim, -psflim, psflim]))
		self.ax2.set_title("2D Point Spread Function")
		self.ax2.set_ylabel("Vertical Position [nm]")
		self.ax2.set_xlabel("Horizontal Position [nm]")
		if pyplot.gci() is not None:
			cbar.set_clim(vmin=0,vmax=np.max(im[:]))
			cbar.draw_all()
		self.fig.tight_layout()
		self.canvas.draw()






if __name__ == "__main__":
	root = tk.Tk()
	GUIApplication(root)#.pack(side="top", fill="both", expand=True)
	root.mainloop()
	










