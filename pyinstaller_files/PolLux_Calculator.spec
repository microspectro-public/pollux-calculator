# -*- mode: python ; coding: utf-8 -*-
# Windows

#from .. import PolLux_Calculator
#version = PolLux_Calculator.__version__


block_cipher = None


a = Analysis(
    ['../PolLux_Calculator.py'],
    pathex=[],
    binaries=[],
    datas=[],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=['PyQt5'],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)

splash = Splash('PolLux_Logo.png',
                binaries=a.binaries,
                datas=a.datas,
				max_img_size=(500,500),
                text_pos=(10, 485),
                text_size=12,
                text_color='black')

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    [],
	splash,
	splash.binaries,
    name='PolLux_Calculator',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
	icon='./PolLux_Logo.ico',
)
