These files are used to compile executables using PyInstaller.

Install PyInstaller with `pip install pyinstaller`.

Compile with the command `pyinstaller PolLux_Calculator.spec`.

The compiled executable will be created in a `dist` directory.